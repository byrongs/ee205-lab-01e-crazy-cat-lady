///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  @Byron Soriano <@byrongs@hawaii.edu>
/// @date    @18 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main( int argc, char* argv[] ) {
	std::cout << "Crazy Cat Lady!" << std::endl ;
        
	std::cout << "Oooooh!" << argv[1] << " you're so cute!" << std::endl ;
   return 0;
}
