/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
/// @file    crazyCatLady.cpp                                                                                           
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  @Byron Soriano <@byrongs@hawaii.edu>
/// @date    @18 Jan 2022
///////////////////////////////////////////////////////////////////////////////

CC     = gcc
CFLAGS = -g -Wall

TARGET = hello

all: $(TARGET)

crazyCatLady: crazyCatLady.cpp
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o

